#!/usr/bin/python3

import webapp
from urllib.parse import unquote

urlDic = {}  # diccionario para las URLs --> 'recurso': 'URL'

formulario = """
 <form action ="" method ="POST">
    <h2>Introduzca una URL y su recurso acortado:</h2>
    Url: <input type="text" name="url">
    Acortar: <input type="text" name="short"><br><br>
    <input type="submit" value="Acortar"><br>
 </form>
"""


class Acortador_URLs(webapp.webApp):
    def parse(self, request):
        method, resource, _ = request.split(' ', 2)
        # Trocea los dos primeros espacios en blanco y
        # luego guarda las 2 primeras palabras
        print(request)
        body = unquote(request.split('\r\n\r\n', 1)[1])  # Trocea por /r/n/r/n
            # solo una vez y acaba (almacena el segundo campo, que sera el body)
        return method, resource, body

    def process(self, parsedRequest):
        method, resource, body = parsedRequest
        print('\r\nEl metodo es: ' + method)
        print('El recurso es: ' + resource)
        print('El cuerpo es: ' + body)

        if method == "GET":
            if resource == "/":  # Recurso / (el recurso principal)
                print(str(urlDic))
                return ("200 OK", "<html><body><h1>Acortador de URLs </h1>" +
                        formulario +
                        "<br><br>" + "URLs almacenadas: "
                        + str(urlDic) + "</body></html>")
            else:
                recurso = resource.split("/")[1]  # Recurso sin / para
                # hacer la comprobacion con el diccionario
                if recurso in urlDic:  # Si el recurso ya esta como clave, entonces
                    # se hace una redireccion (para URLs ya acortadas)
                    print("HTTP REDIRECT: " + str(urlDic[recurso]))
                    return ("302 Found\r\nLocation: " +
                            urlDic[recurso], "")  # Redirección
                else:  # Redireccion para el resto de recursos (de localhost)
                    return ("404 NOT FOUND", "<html><body><h1>Recurso no encontrado.</h1>" +
                            "<a href=http://localhost:1234/>Volver</a></body></html>")

        if method == "POST":
            url_qs = body.split("=")[1].split("&")[0]  # Sacar url de la qs
            short_qs = body.split("=")[2]  # Sacar recurso short de la qs
            if url_qs == "":  # Con qs vacía en el campo url
                return ("400 Bad Request", "<html><body><h1>Por favor, introduzca una URL.</h1>" +
                        "<a href=http://localhost:1234/>Volver</a></body></html>")
            elif short_qs == "":  # Con qs vacía en el campo short
                return ("400 Bad Request", "<html><body><h1>Por favor, introduzca un recurso para acortar.</h1>" +
                        "<a href=http://localhost:1234/>Volver</a></body></html>")
            else:  # Si hay URL y recurso para acortar en la qs
                contenido = unquote(url_qs)  # URL extraida de qs decodificada
                print("Cuerpo decodificado: " + contenido)
                # Manejo de inicios de url varios
                if contenido.startswith("http://www"):
                    contenido = "http://" + str(contenido.split("www.")[-1])
                elif contenido.startswith("http://"):
                    contenido = "http://" + str(contenido.split("//")[-1])
                elif contenido.startswith("https://www"):
                    contenido = "https://" + str(contenido.split("www.")[-1])
                elif contenido.startswith("https://"):
                    contenido = "https://" + str(contenido.split("//")[1])
                else:
                    if contenido.startswith("www."):
                        contenido = "https://" +\
                                    str(contenido.split("www.")[-1])
                        # Se tiene en cuenta si empieza
                        # por www. para añadir http://
                    else:
                        contenido = "https://" + contenido
                        # Casos con la url del sitio sin nada delante
                        # (se añade https://)

                urlDic[short_qs] = contenido  # Guarda como valor la
                # URL y como clave asociada el recurso

                return ("200 OK", "<html><body><h1>URL acortada: " +
                        "<a href='" + str(contenido) +
                        "'>" + "http://localhost:1234/" +
                        str(short_qs) + "</a></h1><br><h2>URL original: " +
                        "<a href='" + str(contenido) + "'>" + str(contenido) +
                        "</a></h2><br><br><a href=http://localhost:1234/>Volver</a></body></html>")


if __name__ == "__main__":
    testWebApp = Acortador_URLs("localhost", 1234)
